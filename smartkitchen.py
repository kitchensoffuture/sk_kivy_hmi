import kivy
import serial 
import pycurl
from io import BytesIO 
import serialpi
import multiprocessing
from multiprocessing import Manager

from time import time
from os.path import dirname, join
from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.dropdown import DropDown
from kivy.uix.spinner import Spinner
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.base import runTouchApp
from kivy.properties import ObjectProperty, NumericProperty, StringProperty, ListProperty, BooleanProperty
from kivy.uix.popup import Popup
from kivy.uix.slider import Slider

sp = None

def button_callback(instance):
    print('Button <%s> pressed' % instance.text)

class ShowcaseScreen(Screen):

    def add_widget(self, *args):
        if 'content' in self.ids:
            return self.ids.content.add_widget(*args)
        return super(ShowcaseScreen, self).add_widget(*args)

class A(Widget):

    button_START = ObjectProperty(None) # This way it referenes to the button defined in .kv file and this identifier functions as normal button
    button_STOP = ObjectProperty(None)  

    def __init__(self, **kwargs):
        super(A, self).__init__()    # This is necessary as parent class defines some elements used by add_widget menthod
        self.button_START.bind(on_press=button_callback)    
        self.button_STOP.bind(on_press=button_callback)    

class smartApp(App):
    
    index = NumericProperty(-1)
    i = 0
    dict_ = {}
    selected_dish = "None"
    event = None

    def build(self):
        self.title = 'Smart Kitchen'
        self.screens = {}
        self.available_screens = ['Recipeselect', 'Manual', 'Automatic', 'Finish']
        self.screen_names = self.available_screens
        curdir = dirname(__file__)
        print(curdir, __file__)
        print(self.available_screens)
        self.available_screens = [join(curdir, 'screens', 
            '{}.kv'.format(fn).lower()) for fn in self.available_screens]
        print('***', self.available_screens)
        self.go_next_screen()
        self.event = Clock.schedule_interval(self.update, 1.0/20.0)
        
        # popup = Popup(title='Test popup',
        #     content=Label(text='Hello world'),
        #     auto_dismiss=False,
        #     size_hint=(None, None), size=(400, 400))
        # #popup.add_widget(Label(text='Hello world'))
        # #popup.add_widget(Slider())
        # popup.open()

        #return A()

    def go_next_screen(self):
        self.index = (self.index+1) % len(self.available_screens)
        print('index is %d' % self.index)
        screen = self.load_screen(self.index)
        sm = self.root.ids.sm
        sm.switch_to(screen, direction='left')

    def go_prev_screen(self):
        self.index = (self.index-1) % len(self.available_screens)
        print('index is %d' % self.index)
        screen = self.load_screen(self.index)
        sm = self.root.ids.sm
        sm.switch_to(screen, direction='right')

    def load_screen(self, index):
        screen_file = self.available_screens[index]
        print('screen_file is  %s' %screen_file)
        screen = Builder.load_file(screen_file)
        self.dict_.update({screen.name:screen})
        print("dict_ is ", self.dict_)
        if 'dish' in screen.ids:
            screen.ids['dish'].text = self.selected_dish
        return screen

    def update(self, dt):
        self.i = self.i + 1  
        output_queue.put("hi".encode('ascii'))
        if not input_queue.empty():
            message = input_queue.get()
            print(message)
            message1 = message[:2]

            if 'Automatic' in self.dict_:
                self.dict_['Automatic'].ids.qty.text = '3. Qty to be dispensed: {}'.format(message1)

            if 'Manual' in self.dict_:
                self.dict_['Manual'].ids.qty.text = '3. Qty to be dispensed: {}'.format(message1)


        # with serial.Serial('/dev/cu.usbmodem141101', 19200,  timeout=.1) as ser: # open serial port
        #     print('serial name', ser.name)         # check which port was really used
        #     ser.write(b'hello')     # write a string
        #     s = ser.read(1)        # read 5 byte
        #     print(s)

        # b_obj = BytesIO() 
        # crl = pycurl.Curl() 
        # crl.setopt(crl.URL, 'https://wiki.python.org/moin/BeginnersGuide')
        # crl.setopt(crl.WRITEDATA, b_obj)
        # crl.perform() 
        # crl.close()
        # get_body = b_obj.getvalue()
        # print('Output of GET request:\n%s' % get_body.decode('utf8')) 

    def spinner_callback(self, text):
        print('The spinner ', text)
        self.root.ids.ap.title = text
        self.selected_dish = text      

    # def stop(self):
    #     super(smartApp, self).stop(self)
    #     Clock.unschedule(self.event)
    #     sp.close()  # closes serial port
    #     print("closd")
    #     while not input_queue.empty():
    #         input_queue.get()
    #     output_queue.close()
    #     input_queue.close()
    #     sp.terminate()  # OS requests the process to terminates
    #     print("terminated")
    #     sp.join(1)   # waits till process is terminated, lets OS reclaim process resources
    #     print("In stop menthod")
    #     print("The main process terminating")


if __name__ == '__main__':
    # input_queue = multiprocessing.Queue()
    # output_queue = multiprocessing.Queue()

    input_queue = Manager().Queue()
    output_queue = Manager().Queue()

    sp = serialpi.SerialProcess(output_queue, input_queue)
    sp.daemon = True
    sp.start()

    smartApp().run()